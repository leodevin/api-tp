const express = require('express');
// Lodash utils library
const _ = require('lodash');
const axios = require('axios');
const fs = require("fs");
const data = require('../data.json');



const router = express.Router();

function filmExist(id){
    let data = fs.readFileSync('data.json');
    let films = JSON.parse(data);

    for(var i=0; i<films.length;i++){
        if (films[i].id === id){
            return films[i];
        }
    }
    return false;
}


/* GET one movie. */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    let data = fs.readFileSync('data.json');
    let films = JSON.parse(data);

    let film;
    if (film = filmExist(id)){
        // Return user
        res.status(200).json({
            message: 'Film found!',
            film
        });
    } else{
        res.status(200).json({
            message: 'Film not found!'
        });
    }

});

/* GET all the movies */
router.get('/', (req, res) => {
    let data = fs.readFileSync('data.json');
    let films = JSON.parse(data);

    res.status(200).json({
        message: 'All the films below',
        films
    });

});

/* PUT a film in our library */
router.put('/:movie', (req, res) => {

    console.log(req.params.movie);
    // Make a request for a user with a given ID
    axios.get(`http://www.omdbapi.com/?t=${req.params.movie}&apikey=a1300a30`)
        .then(function (response) {
            if(response.data.Title != null) {
                var jsonData = JSON.stringify(data);
                var films = JSON.parse(jsonData);
                var film = {
                    id: response.data.imdbID,
                    movie: response.data.Title,
                    yearOfRelease: parseInt(response.data.Year),
                    duration: response.data.Runtime, // en minutes,
                    poster: response.data.Poster, // lien vers une image d'affiche,
                    actors: response.data.Actors,
                    boxOffice: response.data.BoxOffice, // en USD$,
                    rottenTomatoesScore: parseFloat(response.data.imdbRating)
                };

                films.push(film);

                let dataFilms = JSON.stringify(films, null, 2);
                fs.writeFileSync('data.json', dataFilms);

                res.status(200).json({
                    message: "This film has been added :",
                    film
                });
            }else {
                res.status(200).json({
                    message: "Film not found",
                });

            }
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
});

/* DELETE a film. */
router.delete('/:id', (req, res) => {
    // Get the :id of the user we want to delete from the params of the request
    const { id } = req.params;
    let rawdata = fs.readFileSync('data.json');
    let films = JSON.parse(rawdata);

    films.splice(id, 1);

    let dataFilms = JSON.stringify(films, null, 2);
    fs.writeFileSync('data.json', dataFilms);

    // Return message
    res.json({
        message: `Just removed ${id}`
    });
});

/*
/* UPDATE notation. */
router.post('/:id', (req, res) => {
    // Get the :id of the user we want to update from the params of the request
    const { id } = req.params;
    const { rating } = req.body;

    let rawdata = fs.readFileSync('data.json');
    let films = JSON.parse(rawdata);

    let film;

    for(var i=0; i<films.length;i++){
        if (films[i].id === id){
            films[i].rottenTomatoesScore=parseFloat(rating);
            film=films[i];
        }
    }

    let dataFilms = JSON.stringify(films, null, 2);
    fs.writeFileSync('data.json', dataFilms);

    // Get List of user and return JSON
    res.json({
        message: `Just updated the film with the ID :${id}, ${rating} `,
        film
    });
});

module.exports = router;