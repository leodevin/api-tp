import React from 'react';
import logo from './logo.svg';
import './App.css';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {AxiosProvider, Request, Get, Delete, Head, Post, Put, Patch, withAxios} from 'react-axios'


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movieList: []
        };
    }

    componentDidMount(): void {
        this.getFilms();
    }

    getFilms() {
        const axios = require('axios');
        // Make a request for a user with a given ID
        axios.get('http://localhost:3000/films/')
            .then(response => {
                this.setState(
                    ({movieList: response.data.films})
                );
                console.log(this.state.movieList[0].movie);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(function () {
                // always executed
            });
    }

    render() {
        return (
            <div className="App">
                <div className="container-fluid">
                    <div className="row">
                        {this.state.movieList.map(movie =>
                            <div className={"col-md-3"}>
                                <Card style={{width: '18rem'}}>
                                    <Card.Img variant="top" src={movie.poster}/>
                                    <Card.Body>
                                        <Card.Title>{movie.movie}</Card.Title>
                                        <Card.Text>
                                            ID : {movie.id},
                                            Date de réalisation : {movie.yearOfRelease},
                                            Durée : {movie.duration},
                                            Acteurs : {movie.actors},
                                            Notation RT (/10) : {movie.rottenTomatoesScore}
                                        </Card.Text>
                                        <Button variant="primary" onClick={() => this.getFilms()}>
                                            Actualiser
                                        </Button>
                                    </Card.Body>
                                </Card>
                            </div>)
                        }
                    </div>

                </div>
            </div>
        );
    }

}

export default App;
